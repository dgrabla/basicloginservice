'use strict'

// DGB 2016-09-17 15:36 Node 6.5 STILL doesnt support EC6 style import :(
const express = require('express');
const session = require('express-session')
const expressHandlebars  = require('express-handlebars');
const flash = require('connect-flash');
const expressEjs = require('ejs');
const bodyParser = require('body-parser')
const passport = require('passport'); 
const localPassportStrategy = require('passport-local').Strategy; 
const mongodb = require('mongodb');
const crypto = require('crypto');

var app = express();

mongodb.MongoClient.connect('mongodb://dgrabla:Dak3quidha@ds033126.mlab.com:33126/basic_login_service', (err, db) => {

  if (err) {
    throw err;
  }
  console.log('\n✓ Service connected to Mongo. Setting Express up...\n')


// verify()
// Checks the password hash matches the hash on the database and calls a
// callback
var verify = function verify(username, password, callback) {
  let success = (data) => {
    return callback(null, data)
  };
  let failedAuth = () => {
    return callback(null, false, {message: 'Wrong password'});
  }
  db.collection('users').findOne({username: username},(err, user) => {
    if (err) {
      return callback(err);
    }
    if (user) {
      // DGB 2016-09-17 15:34 Taken from node.js docs, reduced to 5000 iterations
      crypto.pbkdf2(password, user.salt, 1000, 512, 'sha512', (err, key) => {
        if (err) {
          return callback(err);
        }
        if (key.toString('base64') === user.secret) {
          return success(user);
        } else {
          return failedAuth() 
        }
      });
    } else {
      // Reach this point when the username doesnt exist
      // WARNING: We should never disclose the username doesnt exist. 
      // This was pointed out to me on a security audit. 
      return failedAuth() 
    }
  }); 
}

passport.use(new localPassportStrategy(verify))


passport.serializeUser(function(user, done) {
  //done(null, JSON.stringify(user));
  done(null, user._id);
});

passport.deserializeUser(function(userId, done) {
  db.collection('users').findOne({_id: new mongodb.ObjectId(userId)},(err, user) => {
    done(err, user);
  })
});

  app.engine('handlebars', expressHandlebars({}));

  // In production this points out wherever we have the public assets
  // (including the login form)
  app.use(express.static(__dirname + '/../frontend/build/public'));

  app.use(flash());
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(express.cookieParser());
  app.use(session({ secret: 'e54ee21909959ed',  resave: false, saveUninitialized: true }));
  app.use(passport.initialize());
  app.use(passport.session());

  var loggerMiddleware = (req, res, next) => { 
    if(req.isAuthenticated()) {
      console.log('User ' + req.user._id + ' requested ' + req.path + ' ');
    }
    else {
      console.log('Unauthorized user requested ' + req.path);
    }
    next();
  } 
  
  app.post('/login', passport.authenticate('local', {failureFlash: true, failureRedirect: '/login', successRedirect: '/'}));

  app.get('/login', 
    loggerMiddleware,
    (req, res) => {
      res.render(__dirname + '/../frontend/build/public/login.handlebars', {flash: req.flash()});
    }
  );

  app.get('/',
    loggerMiddleware,
    (req,res,next) => {
    if(req.isAuthenticated()) {
      res.render(__dirname + '/../frontend/build/secure/index.handlebars', req.user);
    }
    else {
      res.redirect('/login');
    }
  });

  // DGB 2016-09-18 17:32 Server side logout (could also delete cookie on the
  // client, but it will keep uneccessary open sessions on the server)
  app.get('/logout',
    (req, res) => {
      req.session.destroy()
      req.logout()
      res.redirect('/')
  });
  
  console.log('\n✓ Configuration loaded. Service is ready for client connections\n')
});

app.listen(8080, () => {
  console.log('\n✓ Service started on port 8080. Connecting Datastore... \n')
});
