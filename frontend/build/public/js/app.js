var Login = React.createClass({
getInitialState() {
    return {
      password: '',
      username: ''
    };
  },

  getValidationPasswordState() {
    const length = this.state.password.length;
    if (length > 5) return 'success';
    else if (length > 0) return 'error';
  },

  getValidationUsernameState() {
    // Check if the username already exists is a no no
    // This comes straight from
    // https://github.com/Awenice/matchEmail/blob/master/index.js
    if (!this.state.username.length) {
      return;
    }
    if (/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/i.test(this.state.username)) {
    return 'success';
    }
    return 'error';
  },

  getValidationState() {
    if (this.getValidationUsernameState()==='success') {
      return this.getValidationPasswordState();
    }
    return 'error';
  },

  handlePasswordChange(e) {
    this.setState({ password: e.target.value });
  },
 
  handleUsernameChange(e) {
    this.setState({ username: e.target.value });
  },

  render: function() {

    return (
        <div className="login-container">
        {(() => {
        if (this.props.data.ENV.flash.error.length) {
          return <div className="flashMessages"> ERROR: {this.props.data.ENV.flash.error}</div>
        }
      })()}
   <form
    method="post"
    action="/login">
    <FormGroup
    controlId="formUsername"
          validationState={this.getValidationUsernameState()}
        >
          <ControlLabel>Username</ControlLabel>
          <FormControl
            type="text"
            value={this.state.username}
            placeholder="username"
            onChange={this.handleUsernameChange}
            name="username"
          />
          <FormControl.Feedback />
          <HelpBlock>Must be an email</HelpBlock>
      </FormGroup>
<FormGroup
    controlId="formUsername"
          validationState={this.getValidationPasswordState()}
        >
        <ControlLabel>Password</ControlLabel>
          <FormControl
            type="password"
            value={this.state.password}
            placeholder="(min 6 characters)"
            onChange={this.handlePasswordChange}
            name="password"
          />
          <FormControl.Feedback />
          <HelpBlock>Must have more than 5 characters</HelpBlock>
          <Button bsStyle="primary" type="submit" 
          active={ this.getValidationState()!=='success' }
          disabled={ this.getValidationState()==='error' }
          >Log in</Button>
        </FormGroup>
        </form>
     </div>
     );
  }
});

ReactDOM.render(
  <div>
    <div className="header">Login</div>
    <Login className="root" data={{ENV}} />
  </div>,
  document.getElementById('root')
);
