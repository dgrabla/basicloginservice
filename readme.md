## A basic login form

Simple login form using Express on the backend and React on the frontend. 

Screencast:

![Screencast](screencast.gif)



# Install / Run

Install dependencies
```
> (cd backend; npm install) 
```

Run
```
> (cd backend; npm start) 
```

# Backend

Backend serve content and control user sessions. I used express + passportjs +
mongolab as datastore. 
Static assets (images, js, css which dont need protection) are served directly
by express.

Password hashes are salted with a per-user random salt.

## Login mechanics

When the user hits for the first time `/` the backend creates a new user session. As the user is not authenticated, it is forwarded to the login screen. The login screen is a public site described on the frontend section. Eventually the user completes a form with his username and password and `posts` the data to '/login'. The backend verifies if the user exists, and if the password hash matches. If it doesnt it stores a flash message into the session and returns back the user to the login form with a error message. If the user is authorized the session is updated with the user data and the user is forwarded to `/` which is the secure site.

When the user logouts, he can hit `/logout'. Then the session will be deleted from the datastore (In this case from memory). As the session will not match anymore the user ends unauthorized and returns to the login form. 

# Frontend

## Secure site 
 The actual app, in this case just a welcome index.html. 
This site is not the focus of this challenge, in a real world scenario express
would not serve simple HTML content but a single page application with our
app.

Once the user is authorized, the user can logout on this screen to go back to
the login screen.

## Public site

The public site in a real environment is typically focused on marketing (A/B testing, lots of event
tracking etc) and it links or it has a login form as an entry point to the application. This simple entry form was the focus of this challenge.

I created a very simple React + React Bootstrap site. As this was a unique
component I didnt use the whole react toolset (With router, a build step
etc). Instead the required third party JS is loaded from CDNs and the JS.

When the browser loads the login site, it gets all needed assets and then
React is fired and the component rendered.

The component has both UI and the validation and it is able to POST data to the
backend if the username and password pass validation.


# User Document for the test

The test database on mLab has only a user (on collection users)

- username: dgrabla
- password: SafePassword?


```
{
    "_id": {
        "$oid": "57dd406cdcba0f1bc33916c8"
    },
    "profile": {
        "firstName": "David",
        "familyName": "Grajal"
    },
    "username": "dgrabla@gmail.com",
    "salt": "YWFhYWM2NjNkMjI0MzI4ZWMxMzlhMWUzMzc2MTg1NTZiMTI2ZmQ0ZjIxNzM2NTI0",
    "secret": "xFf2QYXULrYX4veYQ009M4o9DtIdtbdrDF6lBJADUr1KDYy9YNnx6XMfJ8KXDEkX0Z5Q4xgGA6XURMJzixM8RKJrhoUptI156pwaYa7SWVMXRGGGEMr0VGfPSxtpoEcGXL74oLUQ+zk6v9N9XxTCMx2AaJR5UcKri9M78XkIZ/GR6/ibahtDw+4HFOhl1dRTGTivUtkVvqmRZJWRY1U2Hp/PCdw1ytBcy90wEg5IeJ8ZdVbg2pyDS2twIBXQTJREJJdmIEvF4+BKMKY3GvvS8BVwtWag0WMlCfQylRazxfBS5Ejm3/NiF1EjQ3ziX1aIqHfNVlkWrMc+Yf74fxM6VqaRGP2smdrttURhRJpGgU0w1EwtcDTpYz4lLXhrV+ztcpz6/58lr4XbtvnWFBM7PuWEZsk+dOQ5gVXPycUWENR6Y+vHRHsjPcaDr7P7MDKAEGA8UD0PuzDkI0HEmt2YNuT6O/6uWuV5oTs0YGuxoTmAg7pOv3AEudocNth/Bz3f7FAZdkmmuQfmYJ7vQEuksVJcXYFtFmvZLg6GMFJZEJq+/AuxUUs2Li7KYU0k+7TMad0xt2lHnao/iIKB7X6M5phs9xTJON+BzxIzQ2rTKro2ApVcz6ziWSRQKxCQBGQwWNIhMbqxsjOPynda3sbNS0QEIiMbgvcFH4pmsUxlCMM="
}
```


# How to improve from here

- Persistent sessions. Express should store the sessions on a datastore to keep
  sessions persistent across restarts and to be able to have multiple nodes
  serving the application

# What did I learn

- First time using React Bootstrap - Quite awesome, saves a lot of time
- You can already write almost EC6 code on node.js without transpiling
- There should be data validation and sanitization on the server.